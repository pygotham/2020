---
name: Bloomberg
tier: gold
site_url: https://www.techatbloomberg.com/
logo: bloomberg.png
twitter: TechAtBloomberg
---
Bloomberg is building the world's most trusted information network for financial
professionals. Our 6,000+ engineers are dedicated to advancing and building new
systems for the Bloomberg Terminal to solve complex, real-world problems. We
trust our teams to choose the right technologies for the job, and, at Bloomberg,
the answer is often Python. We employ an active community of more than 2,000
Python developers who have their hands in everything from financial analytics
and data science to contributing to open source technologies like Project
Jupyter. We also sponsor and host many events for the Python community.
