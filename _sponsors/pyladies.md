---
name: PyLadies
tier: community
site_url: https://www.pyladies.com
logo: pyladies.svg
twitter: pyladies
---
PyLadies are a group of women developers worldwide who love the Python
programming language. We write code by day or night. Some of us hack on Python
projects on the side, while others work full-time on Python development. But it
doesn't matter. We all just like writing Python code, and that's what brings us
together.
