---
name: InformIT
tier: media
site_url: https://www.informit.com/python
logo: informit.png
twitter: informIT
---
InformIT is your one-stop technology learning resource. Our passion is delivering trusted and
quality content and resources from the authors, creators, innovators, and leaders of technology from
the best imprints in IT including Addison-Wesley. Whether you are looking to improve your Python
programming skills, looking for a go-to reference, or wanting to go deeper into data science, our
collection of books and video training has you covered with works from Zed Shaw, David Beazley,
Raymond Hettinger, Brett Slatkin, Wes Chun, and more.
