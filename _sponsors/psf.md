---
name: Python Software Foundation
tier: community
site_url: https://www.python.org/psf/donations/
logo: psf.svg
twitter: ThePSF
---
The mission of the Python Software Foundation is to promote, protect, and
advance the Python programming language, and to support and facilitate the
growth of a diverse and international community of Python programmers. The
majority of the PSF’s work is focused on empowering and supporting people within
the Python community. The pandemic negatively affected the PSF’s finances with
the cancellation of PyCon 2020’s in-person conference and lower donations. How
can you help? Check out <https://www.python.org/psf/get-involved/> for 20+ ways
you can get involved and support the PSF and Python community!
