---
name: NYC PyLadies
tier: community
site_url: https://nyc.pyladies.com/
logo: nyc-pyladies.png
---
We’re the NYC chapter of PyLadies, an international community of women and
non-binary people in tech.

We host monthly events, talks, and study groups where you can meet other
pythonistas and learn more about programming, data science, web development, and
more.

All skill levels welcome!
