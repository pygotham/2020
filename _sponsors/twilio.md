---
name: Twilio
tier: gold
site_url: https://www.twilio.com/
logo: twilio.svg
twitter: twilio
---
Twilio has provided closed captioning for PyGotham TV.

Twilio powers the future of business communications. Enabling phones, VoIP, and messaging to be
embedded into web, desktop, and mobile software.

Millions of developers around the world have used Twilio to unlock the magic of communications to
improve any human experience.

Twilio has democratized communications channels like voice, text, chat, video, and email by
virtualizing the world’s communications infrastructure through APIs that are simple enough for any
developer to use, yet robust enough to power the world’s most demanding applications.

By making communications a part of every software developer’s toolkit, Twilio is enabling innovators
across every industry — from emerging leaders to the world’s largest organizations — to reinvent how
companies engage with their customers.
