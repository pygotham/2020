---
name: DEFNA
tier: community
site_url: https://www.defna.org/
logo: defna.png
---
DEFNA (Django Events Foundation North America) is a non-profit based in
California, USA, which runs the DjangoCon US conference. Our goal is to help
fund Django events and meetups while promoting Django education and outreach in
North America.
