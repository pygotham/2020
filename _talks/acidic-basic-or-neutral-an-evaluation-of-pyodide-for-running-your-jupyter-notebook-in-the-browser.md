---
duration: 36
presentation_url: null
room: Online
slot: 2020-10-02 11:10:00-04:00
speakers:
- "H\xE5kan Silfvernagel"
title: Acidic, basic or neutral - an evaluation of Pyodide for running your Jupyter
  Notebook in the browser
type: talk
video_url: null
---

Have you ever wanted to run a full data science pipeline in the browser
using python?

Now you can do that by using Pyodide. Pyodide brings you the python runtime
in your browser including your favorite data science libraries such as
NumPy, SciPy and Pandas.

This will provide you with a Jupyter Notebook like experience straight in
the browser!

Join this session to understand more about the capabilities and limitations
that Pyodide provides you for running data science projects in the browser.
