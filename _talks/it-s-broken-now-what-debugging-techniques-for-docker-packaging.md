---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-03 10:05:00-04:00
speakers:
- Itamar Turner-Trauring
title: It's broken, now what? Debugging techniques for Docker packaging
type: talk
video_url: "https://youtu.be/SWHkY3IaCWU"
---

You're packaging your Python application with Docker and something goes
wrong:

* Your build won't even finish.
* Or even worse, your build won't finish and the relevant `pip` logs are
  stored...somewhere in the failed build?
* Your container won't start.
* You can't connect to the running server.


Now what? You need to figure out the problem so you can fix it.

In this demonstration you will learn a variety of debugging techniques to
help you figure out why your Docker packaging is broken. The talk will
involve actual usage of the techniques in a terminal, so you can watch them
in action.
