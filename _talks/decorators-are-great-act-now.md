---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-03 09:10:00-04:00
speakers:
- Andrew Knight
title: Decorators Are Great! Act Now!!
type: talk
video_url: "https://youtu.be/GgYOBF5I0ig"
---

Have you ever seen those “@” tags on top of Python functions and classes?
Those are decorators - functions that wrap around other functions.
Decorators are one of Python’s niftiest language features, and they help
programmers write DRY (Don’t Repeat Yourself) code.

But wait, there's more! In this "infomercial," we’ll learn all about
decorators:

* How they wrap functions
* How to write our own decorators
* How to do cool tricks with arguments, classes, and nesting
* How to decide when decorators are (and aren’t) the right solution


Once you learn how they work and how to write your own, you'll wonder how
you ever lived without them. Act now to use them today!!
