---
duration:
presentation_url: null
room:
slot:
speakers:
- Kamal Abdelrahman
title: Dashing Through Databases
type: talk
video_url: null
---

Dashing through databases, with all this info to learn. Building a web app
in dash, connecting data is what we'll learn.

No ladies and gentleman, this is not another Christmas song that you would
be tirelessly singing along with relatives. This an October treat brought to
you to sweeten up the data analysis process for you and your stakeholder.

With vast amounts of data available to be analyzed, the question becomes,
"How can we work with stakeholders to build tools that empower them to
analyze data?" By empowering, we build a data tool the invites non-technical
stakeholders into the analysis process.

The talk will discuss building a data tool in Python, Dash, and SQL which
takes a non-programmer's approach to analyzing data. This includes building
a tools that allow for high level analysis that require little to no coding
for stakeholders. With the time and energy that data engineers, analysts,
and scientists instill in their day to day lives, its easy to be frustrated
by the surmounting requests for an analysis to be done when more important
tasks require your attention. With this talk, I'll chat with you, yes you
there sitting there on your computer, phone, or tablet about how you can
help make life easier for you and your stakeholders with data tools.

With a bit of communication, sprinkling in a dash of technology, and topped
off with understanding the day-to-day needs of stakeholder, we'll build data
tools that help you and your stakeholder get the most out of your data
