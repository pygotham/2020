---
duration: 15
presentation_url: null
room: Online
slot: 2020-10-03 16:20:00-04:00
speakers:
- Marcus Willock
title: Former Hacker Reviews Iconic Python/Rust Hacking Scenes From Movies &amp; TV
type: talk
video_url: "https://youtu.be/QBN9PpnOGao"
---

Legendary Hacker Lord Nikon most known for his photographic memory and his
“leet” skills takes a look at a variety of hacking scenes from popular media
and examines their authenticity. The scenes under review are ones that
mention Python and Rust existing in the same code base.

This parody of an expert reviewing Movie and TV depictions of their craft
will highlight how to use Rust’s pyo3 crate to create Python extensions
while also commenting on when it is socially acceptable to do so.
