---
duration: 15
presentation_url: null
room: Online
slot: 2020-10-03 13:20:00-04:00
speakers:
- Aaron Bassett
title: PSA - Choose your words carefully
type: talk
video_url: null
---

Despite the commonly held belief, it is not only sticks and stones which can
hurt; the wrong words can cause harm too.

In this public service announcement, we'll look at the etymology of some
common—and often seemingly innocuous—words, phrases, and idioms and why they
should no longer be part of our collective lexicon.

Content Warning: many of the phrases discussed have origins in racism,
misogyny, ableism, and bigotry.
