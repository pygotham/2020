---
duration: 35
presentation_url: null
room: Online
slot: 2020-10-02 10:00:00-04:00
speakers:
- Sangarshanan
title: 'Two is Better than One: A segment on ensembling models'
type: talk
video_url: "https://youtu.be/jqtx-JgKUsM"
---

Is two better than one, or is it just something your grandma says to justify
the extra vegetable she put on your plate. Let's discuss with a late-night-
esque take on the topic where we will explore what ensembling means,
different bagging and boosting methods, and when to use what. This will be
accompanied by a few hit/miss punchlines in an attempt to make Pythonistas
laugh, cringe, and Aha!
