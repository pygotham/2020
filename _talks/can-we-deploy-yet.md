---
duration: 25
presentation_url: null
room: Online
slot: 2020-10-03 09:40:00-04:00
speakers:
- Anastasiia Tymoshchuk
title: Can we deploy yet?
type: talk
video_url: null
---

What happens when your features are done, your MVP is ready and you want to
deploy your first production build? What do you do then? How do you make
your first production build instead of re-using your development one? is
your code ready to handle real user interactions? This talk will show a
production ready checklist for your Python code; what to look for when
creating a production-ready Docker image; what are the differences between
development and production environments and builds. You will see how to deal
with exceptions, logs, and metrics with real-world use cases.
