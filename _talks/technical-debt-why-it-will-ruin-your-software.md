---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-03 11:00:00-04:00
speakers:
- Luan Fonseca
title: 'Technical Debt: Why it will ruin your software'
type: talk
video_url: "https://youtu.be/GqVi_0BA0jo"
---

Technical Debt is one of the main reasons why software fails, we are used to
thinking that it is just bad code. This talk proposes a bigger picture view
of what it means in a Sustainable Software era, how can we identify
bottlenecks that are generating more debt and then how to healthy deal with
it.

We are used to thinking that Technical Debt is just bad/ugly code, with this
talk I want to propose a bigger picture view of what we can call Technical
Debt with a Sustainable Software era, how can we identify bottlenecks that
are generating more debt and then how to deal with it.

After joined on a few ongoing projects as a developer I noticed a lot of
smells for bottlenecks on the product development and delivery phases, those
issues are usually named as Technical Debt issues by the development teams
but not the healthy code is just a reflection of the teams' struggles, when
I say Teams I mean the entire companies’ structures and relations. Those
relations can have a lot of problems and usually, those problems are only
visible on the code written.
