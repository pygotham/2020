---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-02 17:05:00-04:00
speakers:
- Kimberly Fessel
title: Scaled It! Three Contestants Attempt to Reduce Computational Time
type: talk
video_url: null
---

In this episode of the hit reality series _Scaled It!_, three new
contestants compete by taking on classic programming challenge problems.
These beginners will put their Python skills to the test as they attempt to
build scalable code within the allotted time limit.  Our expert judges then
reveal the approaches the participants should have taken including:
vectorization, memoization, and simplifying the problem with mathematical
logic rather than attacking with brute force.  Will the contestants “scale
it” or “snail it”?  Tune in to find out.
