---
duration: 25
presentation_url: null
room: Online
slot: 2020-10-02 09:35:00-04:00
speakers:
- Angeline Protacio
title: The Album Discoverer - An Album Recommendation System
type: talk
video_url: "https://youtu.be/AdU8vkC-FGs"
---

Most music recommendation systems rely heavily on individual tracks. Track
recommendations often drive playlist creation, and are often used to
introduce new artists to a broader audience.  However, musicians often
conceptualize their work as an album, with an intended track order, and
overarching themes. I wanted to create a music recommender that focused on
recommending albums, rather than individual songs, to preserve this
thoughtfulness and provide a more complete experience for the listener, as
the artist intended. In this talk, I'll discuss how I built the Album
Discoverer, a Flask app that uses principal components analysis to distill
an album down to its most salient parts, and recommends albums with the
highest cosine similarity. Along the way, I will show how I validated each
step of the analysis, and end with a demonstration of the app.
