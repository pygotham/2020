---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-02 14:20:00-04:00
speakers:
- Sanyam Khurana
title: Introduction to Metaclasses in Python
type: talk
video_url: null
---

In this talk, we’ll explore metaclasses in Python and learn how to leverage
them for having custom behavior across the codebase. We'll explore the
infamous `type` and how it comes into play in metaclasses. Lastly, we'll
cover how metaclasses can be useful for Abstract Base Classes which comes
from the abc module in the standard library.

A class is a blueprint for an object. It describes how an instance of the
class (ie an object) would behave. A metaclass is the class of a class. It
defines how a class behaves. When you define a class in Python, its
metaclass is called which when combined with __init__ and __new__ methods
allows doing extra things like registering class with some registry or
replace it entirely with something different.

The whole magic comes with type. It is a metaclass in Python, which itself
is a class and it is of its own type. (yes, inception!).
