---
duration: 25
presentation_url: null
room: Online
slot: 2020-10-02 10:45:00-04:00
speakers:
- "Bruno Gon\xE7alves"
title: Visualization with seaborn
type: talk
video_url: "https://youtu.be/ZRv703Z0U7Y"
---

Seaborn is a visualization package that builds on top of matplotlib and
pandas to provides a simple, functional, interface that is capable of
generating sophisticated and beautiful visualizations. In this lecture we
will provide a systematic overview of the way in which seaborn is
structured, how it can be used for data exploration and to produce
publication ready figures and visualizations.
