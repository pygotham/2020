---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-03 12:50:00-04:00
speakers:
- "Miroslav \u0160ediv\xFD"
title: Your Name Is Invalid!
type: talk
video_url: "https://youtu.be/gvMhfIrCT24"
---

People have names. Most people do. People have first names and last names.
Many people do. People have any sorts of names that often don't fit fixed
fields in the forms. These names may contain letters, accented letters, and
other characters, that may cause problems to your code depending on the
encoding you use. They may look differently in uppercase and lowercase, or
may not be case foldable at all. Searching and sorting these names may be
tricky too. And if you design an application, web form, and/or database
dealing with personal names, you'll have to take that into account.

This talk is not about GDPR, but will help you to use the best tools to
handle encoding and locales in Python and prevent your application from
appearing in my talk and in uxfails memes.
