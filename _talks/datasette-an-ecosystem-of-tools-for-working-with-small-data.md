---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-02 12:50:00-04:00
speakers:
- Simon Willison
title: Datasette - an ecosystem of tools for working with Small Data
type: talk
video_url: "https://youtu.be/avy5UFFv2Gw"
---

Big Data tools are powerful, complicated and expensive to run. But what
about Small Data? If your data fits on a USB stick you don't need a Hadoop
cluster to analyze it. Datasette sits at the center of a growing ecosystem
of tools designed to make working with Small Data as productive and
inexpensive as possible.
