---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-02 13:50:00-04:00
speakers:
- Moshe Zadka
title: The Garden of Forking Paths
type: talk
video_url: "https://youtu.be/2RxSoHJcYgg"
---

Explicit may be better than implicit, but brevity is the soul of wit.
Sometimes our code needs to be something "equivalent" in some sense to many
different things. Long `if/elif` chains quickly become tiresome.

How do we make sure the right code gets called, keep our logic readable, and
make modification and refactoring easy?

The Python ecosystem has a whole suite of solutions, some in the standard
library and some in packages available from PyPI. This talk will cover the
alternatives available, when to use them, and how to balance readability
with expressivity.
