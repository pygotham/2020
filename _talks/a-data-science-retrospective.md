---
duration: 50
presentation_url: null
room: Online
slot: 2020-10-02 12:00:00-04:00
speakers:
- Sarah Guido
title: A Data Science Retrospective
type: talk
video_url: null
---

I joined the data science industry six years ago, and so many things have
changed since then. In this talk, I’ll give a highly subjective overview of the
way that the data science industry has evolved over the past six years, based on
my own observed experiences. I’ll discuss the major shifts I’ve seen in both
data tooling and hiring, what I think the future of data science will be, and
the fact that, throughout it all, Python is still the language of choice for
data folks.
