---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-02 15:30:00-04:00
speakers:
- R.Kracekumar
title: Build plugins using Pluggy
type: talk
video_url: "https://youtu.be/BH_52c56WuE"
---

As a programmer, we build applications all the time. The application design
needs to be scalable, manageable, testable, and extensible. The plugins are
one of the ways to make the application components extensible.

The dynamic nature of Python and packaging tools allows the host program to
register the thrid-party plugins and run the plugins during the life-cycle
of the program.

[Pluggy](https://pluggy.readthedocs.io/en/latest/) is a plugin management
and hook calling system developed for the
[pytest](https://docs.pytest.org/en/latest/). It offers plugin management to
any python project using [setup.py entry
points](https://setuptools.readthedocs.io/en/latest/setuptools.html#dynamic-
discovery-of-services-and-plugins) or directly registering the plugin.

Pytest plugins are developed using pluggy. Pluggy is a powerful framework
for developing the plugin architecture.

This talk will provide an introduction to pluggy framework, concepts, and
show how to build an application using pluggy.

### Links in the description

- Pluggy - https://pluggy.readthedocs.io/en/latest/
- Pytest - https://docs.pytest.org/en/latest/
- Setup tools entry point -
  https://setuptools.readthedocs.io/en/latest/setuptools.html#dynamic-
  discovery-of-services-and-plugins
