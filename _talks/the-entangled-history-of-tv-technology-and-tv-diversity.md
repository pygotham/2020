---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-03 16:35:00-04:00
speakers:
- Jennifer Armstrong
title: The Entangled History of TV Technology and TV Diversity
type: talk
video_url: "https://youtu.be/otW2u4XEj0c"
---

Every entertainment medium reflects its technology—its delivery system. But
no medium’s content has reflected its changes in technology as much as
American television has. Who viewers see on screen has always been closely
tied to how they’re getting their TV content. I'll offer a quick tour of
TV’s 70-year history that shows how changes in technology have caused
diversity to wax and wane over time in surprising and telling ways.
