---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-02 16:10:00-04:00
speakers:
- Hayley Denbraver
title: The Detective Has Arrived
type: talk
video_url: "https://youtu.be/m-hBU5OMU_w"
---

Every good TV binge deserves a detective show. Every developer should
consider the security implications of their work. Let's combine the two!
This talk will discuss how to build a security mindset as a software
developer, with help from some of our favorite detectives: Sherlock Holmes,
Miss Marple, and Hercule Poirot.
