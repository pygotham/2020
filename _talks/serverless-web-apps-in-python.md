---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-02 15:00:00-04:00
speakers:
- Sanjay Siddhanti
title: Serverless Web Apps in Python
type: talk
video_url: null
---

Serverless technologies have a lot of people excited - in theory, they offer
infinite scaling with no maintenance. After mostly seeing “hello-world on
Lambda” tutorials, I was curious how it would work for a real application
with a frontend and a database.

This talk reviews my experience using AWS Lambda for full-stack web
applications. If you’re familiar with the theory and want to know more about
how it works in practice, you’re in the right place.
