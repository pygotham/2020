---
duration: 50
presentation_url: null
room: Online
slot: 2020-10-03 12:00:00-04:00
speakers:
- Ayodele Odubela
title: Combating Bias in Machine Learning
type: talk
video_url: null
---

This talk covers how to see the bias in your data and machine learning
models. By giving examples from recent research on biased facial recognition
systems as well as cutting edge explainability algorithms, attendees can
expect clarification on what bias means in machine learning.
