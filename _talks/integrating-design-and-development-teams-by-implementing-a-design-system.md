---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-03 14:20:00-04:00
speakers:
- Mariana Bedran Lesche
- Daniela Falcone
title: Integrating Design and Development teams by implementing a Design System
type: talk
video_url: "https://youtu.be/afC6RhfyqP4"
---

# Abstract

In the software industry, developers, designers and stakeholders should be
working together to achieve the same goals and deliver high quality products
to the final users. To be actually able to work together in an efficient and
harmonic way, though, is a whole other thing. In a team composed by
developers and designers, we were able to mitigate the impact of
communication flaws and concepts divergences in a continuous effort to cover
the blind spots we found on every iteration we went through. In this talk
we're going to share some lessons learned about how to integrate both teams’
work with a solid management process.

# Argument

To deliver a product that involves design - from research to screens and
usability - and its implementation by a development team can be a big
challenge for all parts involved. If we aim to really address users needs
and deliver high quality products and meaningful experiences, it’s important
to have a team aligned and fully integrated.

The interaction between stakeholders, designers and developers normally
involve a lot of friction and divergence of concepts and vocabulary. The
back and forth between development and QA normally take a lot of time and
effort till things get quite right. Besides, people from different
backgrounds tend to have diverging perspectives about the value and the
challenges of implementing an interface. Developers get frustrated when
faced with a non negotiable design, seemingly impossible to implement, while
designers can be angry seeing their conceptions poorly executed and
stakeholders impatient with a product that never gets done.

These problems can be mitigated by a solid management process and a
conscious effort to create a common ground for communication. Following a
path in the direction of the implementation of a Design System can be a
powerful tool to achieve that.

In this talk we're going to share the results of the gradual improvements
adopted by our team in an effort to make these interactions smoother and to
deliver a product with higher quality standards and more aggregated value to
our client. We’ll explain how the processes we introduced to our
interactions were able to impact our productivity while improving the team’s
and the project’s health.
