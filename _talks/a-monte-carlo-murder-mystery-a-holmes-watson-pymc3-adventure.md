---
duration: 25
presentation_url: null
room: Online
slot: 2020-10-02 09:10:00-04:00
speakers:
- Nitya Mandyam
title: 'A Monte Carlo Murder Mystery: A Holmes-Watson-PyMC3 adventure!'
type: talk
video_url: "https://youtu.be/lB_wu6S1-Lk"
---

<i>(The following is intended to be in the style of a movie poster for a 1930's thriller.)</i>

On a beautiful summer’s eve in the coast of Monte Carlo, Count Frick Von
Twist is found dead in his luxurious hotel suite on the heels of a dinner
party celebrating his 80th birthday. In shock and anguish, his adopted
daughter, Viscountess Bae Zhun, calls upon famed detective Shellakybooky
Holmes and his trusted associate Jahnvi Watson to deduce her father’s
killer.

A meticulous Watson interviews the nine dinner guests separately and
examines the Count’s pet, Python, to find probable cause. Holmes quietly
enters his Hamiltonian Mind Castle (HMC) to run simulations of possible
versions of the fated evening’s events and derive posterior distributions of
the probability of each of the guests’ murderous intent. Together, will they
find the Count’s killer with a degree of confidence that would convince a
jury? Will Bae Zhun find peace, convergence and justice amidst all the
uncertainty? Tune in on October 2nd at 9:10 a.m. EST on PyGotham TV to
watch*: <b><i> A Monte Carlo Murder Mystery!</i></b>

<i>*A PyMC3 production directed by an ardent pythonista (who might have read
one too many detective novels) with a little help from friends! </i>

----------------------------------------------------------------------------------------------

##### Motivation behind this Proposal: How to Solve a Murder using Bayesian Inference! #####

For some time now, I have been very interested in giving an introductory
talk on Probabilistic Programming and Bayesian Data Analysis that is geared
towards a general audience of Python programmers. There are, however, a
series of intimidating topics (such as likelihood functions, posterior
distributions, Monte Carlo simulations, Generalized Linear Models, etc) en
route to understanding Bayesian methods in programming that can often dampen
the spirit of a curious learner. These topics have been covered wonderfully
well by experts on the subject (Hugo Bowne-Andersen, Cameron Davidson-Pilon
and Mitzi Morris to name a few) over many hours of conference tutorials and
courses. Deriving inspiration from these as well as drawing upon PyGotham TV
2020’s encouragement to experiment with the television theme, I propose to
give a high level overview of these concepts in an accessible and
entertaining manner using the medium of a film with a murder mystery
plotline.

To this end, I would like to rope in one of my favorite fictional detectives
who also happens to be a true Bayesian**, Sherlock Holmes, to take us
through the steps involved in using Bayesian methods in answering a question
about a dataset (here, for instance, <i>“Who is the murderer?”</i>) and in
updating the inference in the light of new information. The classical
detective novel arc gives a very tangible parallel to making deductions via
Bayesian methods and the visual medium offers exciting possibilities in
terms of conveying complex concepts (detailed further in the Outline),
making the combination very effective for the purpose of reaching a broad
audience.

The talk will thus be in the form of a 1930's style movie interspersed with
relevant infographics. There will be a small amount of narration/voiceover
wherever it may be necessary to convey the plot or underlying concepts. The
amount of code and math depicted on screen will be minimal and an
accompanying [Github repo](https://github.com/nityamd/MonteCarloMurderMystery-PyGothamTV2020) will in turn cover that in further depth. The repo
will contain: the dataset and problem statement; a <i>Jupyter</i> notebook
(or two) that demonstrates how Holmes arrives at the conclusion; a
<i>requirements.txt</i> file for relevant installations; and sufficient
resources and corollary questions that the enterprising audience can play
around with.

**Some quotes from that illustrate Holmes’ Bayesian thinking :<br> “How
often have I said to you that when you have eliminated the impossible,
whatever remains, however improbable, must be the truth?” <i>(The Sign of
the Four)</i><br> “… we balance probabilities and choose the most likely. It
is the scientific use of the imagination … ” <i>(The Hound of the
Baskervilles)</i><br> "Data! Data! Data!" he cried impatiently, "I can't
make bricks without clay." <i>(The Adventure of the Copper Beeches)</i><br>
