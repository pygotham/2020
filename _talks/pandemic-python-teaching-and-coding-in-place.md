---
duration: 34
presentation_url: null
room: Online
slot: 2020-10-03 13:35:00-04:00
speakers:
- Lisa Tagliaferri
title: 'Pandemic Python: Teaching and Coding in Place'
type: talk
video_url: "https://youtu.be/2zk5_o5X4-s"
---

In response to the COVID-19 pandemic, the Computer Science department of
Stanford University rolled out an experimental remote offering of their
established intro to Python programming course, CS106A, entitled “Code in
Place.” A call for volunteer students and teachers resulted in an incredible
learning community comprised of over 10,000 learners and 800 section leaders
from 6 continents, in addition to the Stanford-based professors and teaching
assistants. Unlike much of the demographics of a typical Computer Science
undergraduate course in the United States, Code in Place achieved gender
parity and drew from a diverse international community of teachers and
learners. As there continues to be uncertainty around what the future of
education will look like, both due to the ongoing pandemic and other
societal changes, this talk will share lessons learned through creating a
distributed online learning community from the perspective of a Python
educator and author. We’ll discuss how to center inclusion, how to engage a
global audience, and how to create community in a virtual setting while
empowering new learners to code in Python.
