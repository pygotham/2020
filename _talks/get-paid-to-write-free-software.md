---
duration: 19
presentation_url: null
room: Online
slot: 2020-10-03 15:20:00-04:00
speakers:
- Cristina
title: Get paid to write Free software
type: talk
video_url: "https://youtu.be/UeQU74QtEAA"
---

Free software powers a substantial proportion of all computing technology in
existence, but it’s largely developed and maintained by busy volunteers.
What if technologists could get paid to work on their open source passion
projects? In this talk, we’ll explore a few recent case studies where
funding had a transformative impact on projects within the Python community.
I’ll also introduce the recently-formed Python Software Foundation [Project
Funding work group](https://pyfound.blogspot.com/2020/07/announcing-psf-
project-funding-working.html), and detail how we support technologists in
their search for funding.
