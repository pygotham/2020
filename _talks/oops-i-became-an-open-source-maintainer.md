---
duration: 30
presentation_url: null
room: Online
slot: 2020-10-03 14:50:00-04:00
speakers:
- Mariatta
title: "Oops! I Became an Open Source Maintainer! \U0001F631"
type: talk
video_url: "https://youtu.be/OYudr0tUm6c"
---

I consider myself relatively new to the open source world; my first open
source contribution was in summer of 2016. Pretty soon I found myself being
given commit rights to other people’s open source projects. Being a new open
source maintainer brings a set of unique challenges that I was not fully
prepared for. In this talk, I will share my journey and the things I’ve
learned along the way, and some advice for other aspiring open source
maintainers and contributors.
