---
name: Shamil Turner
talks:
- "Converting Powershell to Python, an onboarding story"
---
Shamil has spent his professional career delivering high performing
solutions for companies such as Boeing, Raytheon, DXC, Disney, etc.
representing a number of Microsoft ISVs, Facebook and most recently Dropbox.
Shamil strongly believes that organizations who invest in SaaS applications
also need to invest in developers who can customize those apps to fit
business needs. 

Shamil lives in Harlem NY with his wife Desiree and his most important
projects, daughter Corbin and son Ellington.
