---
name: Sanyam Khurana
talks:
- "Introduction to Metaclasses in Python"
---
Sanyam is an open-source contributor. He is a bug-triager for CPython and
some of his noticeable contributions are in Django and Mozilla's ecosystem
of projects. He mostly dabbles with Python/Django at his day job making
RESTful APIs that power web and mobile apps.
