---
name: R.Kracekumar
talks:
- "Build plugins using Pluggy"
---
I'm a FOSS enthusiast and programmer with an interest in back-end
technologies. I have worked on web-applications, data analysis, and machine
learning.
