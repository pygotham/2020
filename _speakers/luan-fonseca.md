---
name: Luan Fonseca
talks:
- "Technical Debt: Why it will ruin your software"
---
Luan is a Brazilian Full Stack Developer that works with Python and Django
since 2012.
