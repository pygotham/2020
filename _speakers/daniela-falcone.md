---
name: Daniela Falcone
talks:
- "Integrating Design and Development teams by implementing a Design System"
---
User Experience Designer with experience in Digital Products and background
in Graphic Design. For her, designers are innate problem solvers capable to
impact and influence others for a positive change in team collaboration.
That translates as experiences in the most divers fields such as R&D on
augmented reality and IoT, micro-interactions for games, end-to-end
healthcare solutions, and currently custom digital software at Labcodes.
