---
name: Håkan Silfvernagel
talks:
- "Acidic, basic or neutral - an evaluation of Pyodide for running your Jupyter Notebook in the browser"
---
Håkan holds a Master of Science degree in Electrical Engineering and in
addition, he holds a Master’s degree in Leadership and Organizational
behavior. He has also taken courses on university level in psychology,
interaction design and human-computer interaction. He has 20 years’
experience of software development in various positions such as developer,
tester, architect, project manager, scrum master, practice manager and team
lead.

Håkan is Chairman of the local chapter of the Norwegian .NET User Group Oslo
(NNUG) and is active as an Ambassador for Oslo.AI the local chapter for the
global City.AI community.

Håkan is a Microsoft Most Valuable Professional (MVP) in AI.

Currently Håkan is working as Manager AI and Big Data at Miles AS, a
Norwegian consultancy company.
