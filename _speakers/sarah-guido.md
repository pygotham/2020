---
name: Sarah Guido
talks:
- "A Data Science Retrospective"
---
Sarah is the Lead Data Scientist at Scoir, where she helps empower students’
college decisions through data. She is an accomplished conference speaker and
O'Reilly Media author, and enjoys making data science as accessible as possible
to a broad audience. Sarah attended graduate school at the University of
Michigan's School of Information.
