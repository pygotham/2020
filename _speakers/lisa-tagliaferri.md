---
name: Lisa Tagliaferri
talks:
- "Pandemic Python: Teaching and Coding in Place"
---
Lisa Tagliaferri is Senior Manager of Developer Education at the cloud
computing company DigitalOcean. Formerly a researcher and educator at
Harvard, MIT, and the City University of New York, Lisa is the author of the
popular books _How To Code in Python_ and _Python Machine Learning
Projects_, which are both available as open access texts to support new
Pythonistas as they learn to code.
