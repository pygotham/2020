---
name: Jennifer Armstrong
talks:
- "The Entangled History of TV Technology and TV Diversity"
---
Jennifer Keishin Armstrong is the New York Times bestselling author of
Seinfeldia: How a Show About Nothing Changed Everything; a history of The
Mary Tyler Moore Show, Mary and Lou and Rhoda and Ted; and Sex and the City
and Us: How Four Single Women Changed the Way We Think, Live, and Love. She
spent a decade on staff at Entertainment Weekly and has since written for
many publications, including BBC Culture, The New York Times Book Review,
Vice, New York magazine, and Billboard. She also speaks about pop culture
history and creativity.
