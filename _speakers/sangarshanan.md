---
name: Sangarshanan
talks:
- "Two is Better than One: A segment on ensembling models"
---
I am called Sangarshanan, I am a human. I dabble with data. I am into
anything that begins with Data: Data science, Data analytics , Data
engineering, you name it. I have built Machine learning models and Deep
Neural Networks. I am currently exploring ETL's and data pipelines with
airflow. I am also working on some really cool GIS visualizations with Qgis.
I make dank memes and love standup. I also love talking about Astrophysical
paradoxes and Science Fiction.
