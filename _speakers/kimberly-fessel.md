---
name: Kimberly Fessel
talks:
- "Scaled It! Three Contestants Attempt to Reduce Computational Time"
---
Kimberly Fessel is a data science bootcamp instructor at Metis in New York
City. Her professional interests include natural language processing, data
visualization, and data storytelling. Kimberly’s enthusiasm for teaching
comes from her days as an academic. She holds a Ph.D. in applied mathematics
from Rensselaer Polytechnic Institute and completed a postdoctoral
fellowship in math biology at the Ohio State University.
