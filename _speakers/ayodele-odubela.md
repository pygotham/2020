---
name: Ayodele Odubela
talks:
- "Combating Bias in Machine Learning"
---
Ayodele Odubela is a Data Scientist working on driver risk mitigation at
SambaSafety in Denver, CO. She earned a Master's degree in Data Science
after transitioning to tech from social media marketing. She's created
algorithms that predict consumer segment movement, goals in hockey, and the
location of firearms using radio frequency sensors. Ayodele is passionate
about using tech to improve the lives of marginalized people.
