---
title: "Keynote: Sarah Guido"
date: 2020-09-08 00:00:00 -0400
image: /uploads/posts/sarah-guido.jpg
excerpt_separator: <!--more-->
---
Sarah is the Lead Data Scientist at Scoir, where she helps empower students’
college decisions through data. <!--more--> She is an accomplished conference
speaker and O'Reilly Media author, and enjoys making data science as accessible
as possible to a broad audience. Sarah attended graduate school at the
University of Michigan's School of Information.
