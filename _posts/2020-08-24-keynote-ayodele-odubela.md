---
title: "Keynote: Ayodele Odubela"
date: 2020-08-24 00:00:00 -0400
image: /uploads/posts/ayodele-odubela.jpg
excerpt_separator: <!--more-->
---
Ayodele Odubela is a Data Scientist working on driver risk mitigation at
SambaSafety in Denver, CO. <!--more--> She earned a Master's degree in Data
Science after transitioning to tech from social media marketing. She's created
algorithms that predict consumer segment movement, goals in hockey, and the
location of firearms using radio frequency sensors. Ayodele is passionate about
using tech to improve the lives of marginalized people. 
