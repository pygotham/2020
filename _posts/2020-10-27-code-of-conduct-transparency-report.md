---
title: Code of Conduct Transparency Report
date: 2020-10-27 00:00:00 -0400
excerpt_separator: <!--more-->
---
PyGotham strives to be a friendly and welcoming environment, and we take our
code of conduct seriously. In the spirit of transparency, we publish a summary
of the reports we received this year.
<!--more-->
We appreciate those reports, and we encourage anyone who witnesses a code of
conduct violation to report it via the methods listed at
<{{ site.url }}{% link about/code-of-conduct.md %}>. PyGotham TV had one
code of conduct report made to the organizers. Anonymized details of the
report are below.
- A talk included jokes at the expense of other people based on their age, social
  media usage, and programming backgrounds. Conference staff contacted the
  speaker and explained that these comments did not foster an inclusive
  environment and did not add substantial value to the presentation. The speaker
  acknowledged this and committed to being more mindful in future talks. The
  talk was included in the conference with the jokes in question edited out by 
  conference staff.
