---
title: Talk list now available!
date: 2020-08-31 10:00:00 -0400
excerpt_separator: <!--more-->
---

The PyGotham TV 2020 [talk list](/talks/) is now available. It features 33
talks spread across October 2nd and 3rd.

<!--more-->

This year's program features a single track with more than 35 speakers,
hosts, and contestants giving talks covering a wide array of topics, from
application design to data science to natural language processing to web
development. Stay tuned for announcements and details about our keynote
speakers, as well.

We're really excited about this year's talks and hope to have a schedule
ready in the next few weeks.

Thanks to everyone who proposed a talk, voted on talks, or signed up for
the program committee.
